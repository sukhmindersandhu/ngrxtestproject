import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { UiHomeModule } from '@myworkspace/ui-home'
import { LayoutComponent } from '@myworkspace/ui-home';
import { Routes, RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { NxModule } from '@nrwl/angular';
import { StoreModule } from '@ngrx/store';

const routes: Routes = [
  { path: '', redirectTo: 'layout', pathMatch: 'full' },
  { path: 'layout', component: LayoutComponent},
  { path: '', redirectTo: '/layout', pathMatch: 'full' },
  { path: '**', redirectTo: '/layout', pathMatch: 'full' }  // Not Found
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule, 
    RouterModule.forRoot(routes, { enableTracing: false }),
    UiHomeModule,
    NxModule.forRoot(),
    StoreModule.forRoot([]),
    EffectsModule.forRoot([]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
