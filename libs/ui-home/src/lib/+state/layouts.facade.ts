import { Injectable } from '@angular/core';

import { select, Store, Action } from '@ngrx/store';

import * as fromLayouts from './layouts.reducer';
import * as LayoutsSelectors from './layouts.selectors';
import { loadLayouts, loadLayoutsSuccess, loadLayoutsSwitchMap as loadLayoutsSwitchMap } from './layouts.actions';

@Injectable()
export class LayoutsFacade {
  loaded$ = this.store.pipe(select(LayoutsSelectors.getLayoutsLoaded));
  allLayouts$ = this.store.pipe(select(LayoutsSelectors.getAllLayouts));
  selectedLayouts$ = this.store.pipe(select(LayoutsSelectors.getSelected));

  testMessageForLayoutComponent = "Message from Layout Facade";

  constructor(private store: Store<fromLayouts.LayoutsPartialState>) {}

  dispatch(action: Action) {
    this.store.dispatch(action);
  }

  loadAll() {
    this.dispatch(loadLayouts());
  }

  loadAllSwitchMap() {
    this.dispatch(loadLayoutsSwitchMap());
  }
}
