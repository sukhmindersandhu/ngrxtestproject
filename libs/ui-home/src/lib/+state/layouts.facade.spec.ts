import { NgModule } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { readFirst } from '@nrwl/angular/testing';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule, Store } from '@ngrx/store';

import { NxModule } from '@nrwl/angular';

import { LayoutsEntity } from './layouts.models';
import { LayoutsEffects } from './layouts.effects';
import { LayoutsFacade } from './layouts.facade';

import * as LayoutsSelectors from './layouts.selectors';
import * as LayoutsActions from './layouts.actions';
import {
  LAYOUTS_FEATURE_KEY,
  State,
  initialState,
  reducer
} from './layouts.reducer';

interface TestSchema {
  layouts: State;
}

describe('LayoutsFacade', () => {
  let facade: LayoutsFacade;
  let store: Store<TestSchema>;
  const createLayoutsEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`
    } as LayoutsEntity);

  beforeEach(() => {});

  describe('used in NgModule', () => {
    beforeEach(() => {
      @NgModule({
        imports: [
          StoreModule.forFeature(LAYOUTS_FEATURE_KEY, reducer),
          EffectsModule.forFeature([LayoutsEffects])
        ],
        providers: [LayoutsFacade]
      })
      class CustomFeatureModule {}

      @NgModule({
        imports: [
          NxModule.forRoot(),
          StoreModule.forRoot({}),
          EffectsModule.forRoot([]),
          CustomFeatureModule
        ]
      })
      class RootModule {}
      TestBed.configureTestingModule({ imports: [RootModule] });

      store = TestBed.get(Store);
      facade = TestBed.get(LayoutsFacade);
    });

    /**
     * The initially generated facade::loadAll() returns empty array
     */
    it('loadAll() should return empty list with loaded == true', async done => {
      try {
        let list = await readFirst(facade.allLayouts$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        facade.dispatch(LayoutsActions.loadLayouts());

        list = await readFirst(facade.allLayouts$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });

    /**
     * Use `loadLayoutsSuccess` to manually update list
     */
    it('allLayouts$ should return the loaded list; and loaded flag == true', async done => {
      try {
        let list = await readFirst(facade.allLayouts$);
        let isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(0);
        expect(isLoaded).toBe(false);

        facade.dispatch(
          LayoutsActions.loadLayoutsSuccess({
            layouts: [createLayoutsEntity('AAA'), createLayoutsEntity('BBB')]
          })
        );

        list = await readFirst(facade.allLayouts$);
        isLoaded = await readFirst(facade.loaded$);

        expect(list.length).toBe(2);
        expect(isLoaded).toBe(true);

        done();
      } catch (err) {
        done.fail(err);
      }
    });
  });
});
