import { createAction, props } from '@ngrx/store';
import { LayoutsEntity } from './layouts.models';

export const loadLayouts = createAction('[Layouts] Load Layouts');

export const loadLayoutsSwitchMap = createAction('[Layouts] Load Layouts Merge Map');

export const loadLayoutsSuccess = createAction(
  '[Layouts] Load Layouts Success',
  props<{ layouts: LayoutsEntity }>()
);

export const loadLayoutsFailure = createAction(
  '[Layouts] Load Layouts Failure',
  props<{ error: any }>()
);
