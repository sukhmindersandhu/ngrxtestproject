import { LayoutsEntity } from './layouts.models';
import * as LayoutsActions from './layouts.actions';
import { State, initialState, reducer } from './layouts.reducer';

describe('Layouts Reducer', () => {
  const createLayoutsEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`
    } as LayoutsEntity);

  beforeEach(() => {});

  describe('valid Layouts actions', () => {
    it('loadLayoutsSuccess should return set the list of known Layouts', () => {
      const layouts = [
        createLayoutsEntity('PRODUCT-AAA'),
        createLayoutsEntity('PRODUCT-zzz')
      ];
      const action = LayoutsActions.loadLayoutsSuccess({ layouts });

      const result: State = reducer(initialState, action);

      expect(result.loaded).toBe(true);
      expect(result.ids.length).toBe(2);
    });
  });

  describe('unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });
});
