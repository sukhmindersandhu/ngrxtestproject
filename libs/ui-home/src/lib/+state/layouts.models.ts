/**
 * Interface for the 'Layouts' data
 */
export interface LayoutsEntity {
  testMessage: string;
  id: string | number; // Primary ID
}
