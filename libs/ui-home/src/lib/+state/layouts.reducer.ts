import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import * as LayoutsActions from './layouts.actions';
import { LayoutsEntity } from './layouts.models';

export const LAYOUTS_FEATURE_KEY = 'layouts';

export interface State extends EntityState<LayoutsEntity> {
  testMessage: string;
  selectedId?: string | number; // which Layouts record has been selected
  loaded: boolean; // has the Layouts list been loaded
  error?: string | null; // last none error (if any)
}

export interface LayoutsPartialState {
  readonly [LAYOUTS_FEATURE_KEY]: State;
}

export const layoutsAdapter: EntityAdapter<LayoutsEntity> = createEntityAdapter<
  LayoutsEntity
>();

export const initialState: State = layoutsAdapter.getInitialState({
  // set initial required properties
  testMessage: null,
  loaded: false
});

const layoutsReducer = createReducer(
  initialState,
  on(LayoutsActions.loadLayouts, state => ({
    ...state,
    loaded: false,
    testMessage: state.testMessage,
    error: null
  })),
  // on(LayoutsActions.loadLayoutsSuccess, (state, { layouts }) =>
  //   layoutsAdapter.addAll(layouts, {
  //      ...state, testMessage: layouts.testMessage, loaded: true })
  // ),
  on(LayoutsActions.loadLayoutsFailure, (state, { error }) => ({
    ...state,
    error
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return layoutsReducer(state, action);
}
