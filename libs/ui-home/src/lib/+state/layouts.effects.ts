import { Injectable } from '@angular/core';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import { HttpClient } from '@angular/common/http';
import { tap, concatMap, map, catchError, switchMap } from 'rxjs/operators';
import { of, Observable, from, pipe } from 'rxjs';

import * as fromLayouts from './layouts.reducer';
import * as LayoutsActions from './layouts.actions';
import { LayoutsEntity } from './layouts.models';

@Injectable()
export class LayoutsEffects {


loadLayoutSwitchMap$ = createEffect(() =>
    this.dataPersistence.fetch(LayoutsActions.loadLayoutsSwitchMap, {
      run: (
        action: ReturnType<typeof LayoutsActions.loadLayoutsSwitchMap>,
        state: fromLayouts.LayoutsPartialState
      ) => {
        // Your custom service 'load' logic goes here. For now just return a success action...
        pipe(
          switchMap(() => this.getData()),
          map(x => {
                  return LayoutsActions.loadLayoutsSuccess({ layouts: x });
              })
          );
      },

      onError: (
        action: ReturnType<typeof LayoutsActions.loadLayoutsSwitchMap>,
        error
      ) => {
        console.error('Error', error);
        return LayoutsActions.loadLayoutsFailure({ error });
      }
    })
  );


  loadLayouts$ = createEffect(() =>
    this.dataPersistence.fetch(LayoutsActions.loadLayouts, {
      run: (
        action: ReturnType<typeof LayoutsActions.loadLayouts>,
        state: fromLayouts.LayoutsPartialState
      ) => {
        // Your custom service 'load' logic goes here. For now just return a success action...
        this.getData()
        .pipe(
        map(x => {
                return LayoutsActions.loadLayoutsSuccess({ layouts: x });
            })
        );
      },

      onError: (
        action: ReturnType<typeof LayoutsActions.loadLayouts>,
        error
      ) => {
        console.error('Error', error);
        return LayoutsActions.loadLayoutsFailure({ error });
      }
    })
  );

  getData() {
    return this.http.get<LayoutsEntity>('http://test.co.uk/aboutme')
    .pipe(
      map((x)=> {
        return x;
      })
    )
  }

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<fromLayouts.LayoutsPartialState>,
    private http: HttpClient
  ) {}
}
