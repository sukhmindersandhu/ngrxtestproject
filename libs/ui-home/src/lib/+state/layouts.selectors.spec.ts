import { LayoutsEntity } from './layouts.models';
import { State, layoutsAdapter, initialState } from './layouts.reducer';
import * as LayoutsSelectors from './layouts.selectors';

describe('Layouts Selectors', () => {
  const ERROR_MSG = 'No Error Available';
  const getLayoutsId = it => it['id'];
  const createLayoutsEntity = (id: string, name = '') =>
    ({
      id,
      name: name || `name-${id}`
    } as LayoutsEntity);

  let state;

  beforeEach(() => {
    state = {
      layouts: layoutsAdapter.addAll(
        [
          createLayoutsEntity('PRODUCT-AAA'),
          createLayoutsEntity('PRODUCT-BBB'),
          createLayoutsEntity('PRODUCT-CCC')
        ],
        {
          ...initialState,
          selectedId: 'PRODUCT-BBB',
          error: ERROR_MSG,
          loaded: true
        }
      )
    };
  });

  describe('Layouts Selectors', () => {
    it('getAllLayouts() should return the list of Layouts', () => {
      const results = LayoutsSelectors.getAllLayouts(state);
      const selId = getLayoutsId(results[1]);

      expect(results.length).toBe(3);
      expect(selId).toBe('PRODUCT-BBB');
    });

    it('getSelected() should return the selected Entity', () => {
      const result = LayoutsSelectors.getSelected(state);
      const selId = getLayoutsId(result);

      expect(selId).toBe('PRODUCT-BBB');
    });

    it("getLayoutsLoaded() should return the current 'loaded' status", () => {
      const result = LayoutsSelectors.getLayoutsLoaded(state);

      expect(result).toBe(true);
    });

    it("getLayoutsError() should return the current 'error' state", () => {
      const result = LayoutsSelectors.getLayoutsError(state);

      expect(result).toBe(ERROR_MSG);
    });
  });
});
