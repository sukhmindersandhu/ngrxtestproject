import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  LAYOUTS_FEATURE_KEY,
  State,
  LayoutsPartialState,
  layoutsAdapter
} from './layouts.reducer';

// Lookup the 'Layouts' feature state managed by NgRx
export const getLayoutsState = createFeatureSelector<
  LayoutsPartialState,
  State
>(LAYOUTS_FEATURE_KEY);

const { selectAll, selectEntities } = layoutsAdapter.getSelectors();

export const getLayoutsLoaded = createSelector(
  getLayoutsState,
  (state: State) => state.loaded
);

export const getLayoutsError = createSelector(
  getLayoutsState,
  (state: State) => state.error
);

export const getAllLayouts = createSelector(getLayoutsState, (state: State) =>
  selectAll(state)
);

export const getLayoutsEntities = createSelector(
  getLayoutsState,
  (state: State) => selectEntities(state)
);

export const getSelectedId = createSelector(
  getLayoutsState,
  (state: State) => state.selectedId
);

export const getSelected = createSelector(
  getLayoutsEntities,
  getSelectedId,
  (entities, selectedId) => selectedId && entities[selectedId]
);
