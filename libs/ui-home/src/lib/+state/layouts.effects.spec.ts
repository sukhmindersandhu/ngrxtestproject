import { TestBed, async } from '@angular/core/testing';

import { Observable } from 'rxjs';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';

import { NxModule, DataPersistence } from '@nrwl/angular';
import { hot } from '@nrwl/angular/testing';

import { LayoutsEffects } from './layouts.effects';
import * as LayoutsActions from './layouts.actions';

describe('LayoutsEffects', () => {
  let actions: Observable<any>;
  let effects: LayoutsEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NxModule.forRoot()],
      providers: [
        LayoutsEffects,
        DataPersistence,
        provideMockActions(() => actions),
        provideMockStore()
      ]
    });

    effects = TestBed.get(LayoutsEffects);
  });

  describe('loadLayouts$', () => {
    it('should work', () => {
      actions = hot('-a-|', { a: LayoutsActions.loadLayouts() });

      const expected = hot('-a-|', {
        a: LayoutsActions.loadLayoutsSuccess({ layouts: [] })
      });

      expect(effects.loadLayouts$).toBeObservable(expected);
    });
  });
});
