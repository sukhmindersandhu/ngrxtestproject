import { Component, OnInit } from '@angular/core';
import { LayoutsFacade } from '../+state/layouts.facade';
import { HttpClient } from '@angular/common/http';
import { LayoutsEntity } from '../+state/layouts.models';

@Component({
  selector: 'myworkspace-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.less']
})
export class LayoutComponent implements OnInit {
  layout: LayoutsEntity;

  constructor(public layoutsFacade: LayoutsFacade,
    private http: HttpClient) { 
      http.get<LayoutsEntity>('http://test.co.uk/aboutme').subscribe( res => {
        this.layout = res;
    });
  }

  ngOnInit(): void {
  }
}
