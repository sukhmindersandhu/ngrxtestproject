import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromLayouts from './+state/layouts.reducer';
import { LayoutsEffects } from './+state/layouts.effects';
import { LayoutsFacade } from './+state/layouts.facade';
import { HttpMockRequestInterceptor } from './interceptor/interceptor.mock';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forFeature(
      fromLayouts.LAYOUTS_FEATURE_KEY,
      fromLayouts.reducer
    ),
    EffectsModule.forFeature([LayoutsEffects])
  ],
  declarations: [LayoutComponent],
  exports: [LayoutComponent],
  providers: [LayoutsFacade,
    {
        provide: HTTP_INTERCEPTORS,
        useClass: HttpMockRequestInterceptor,
        multi: true
    }
  ]
})
export class UiHomeModule {}
